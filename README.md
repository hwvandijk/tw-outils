# tw-outils #

A simple plugin for tiddlywiki with some (personal) utilities.

## Why tw-outils ##

I consider using the [Tamasha](https://kookma.github.io/TW-Tamasha/) plugin for presentations. Coming from the world of LaTeX beamer class I was looking for a way to create sections/subsections etc and their respective bread crumbs.


## How tw-outils ##

Here you find here the source files:

* plugins/* contains the plugin file that usually reside in TW5/plugins/hydiga/diagram
* tiddlers/* the tiddlers used in the demonstration tiddlywiki

## Demonstration ##

A static demonstration is avaiable at: [bitbucket.io](https://hwvandijk.bitbucket.io/tw-outils/)

## Installing the plugin ##

You can drag and drop the plugin from [dataframe tiddlywiki](https://hwvandijk.bitbucket.io/tw-outils/).

* Open the configuration,
* select plugins, and
* drag and drop outils plugin.

